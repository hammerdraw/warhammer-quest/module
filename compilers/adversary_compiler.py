from hammerdraw.compilers import CompilerBase
from hammerdraw.modules.core import ImageModule, TextModule
from hammerdraw.modules.warhammer_quest.modules import StatsModule, BehaviourTableModule, WeaponsModule, AdversaryRulesModule

class AdversaryCompiler(CompilerBase):
    module_name = 'warhammer_quest'
    compiler_type = "adversary"
    behaviour_table_height = None
    
    modules = \
    [
        ImageModule,
        (TextModule, { 'name': "name", 'scale_field': "titleFontSizeScale" } ),
        (TextModule, { 'name': "description", 'scale_field': "descriptionFontSizeScale", 'multiline': True } ),
        StatsModule,
        WeaponsModule,
        BehaviourTableModule,
        AdversaryRulesModule,
    ]
    
    def _get_base_filename(self):
        return super()._get_base_filename() \
            .replace('{weaponsCount}', str(len
        (self.raw['weapons'])))
