from hammerdraw.compilers import CompilerBase
from hammerdraw.modules.core import ImageModule, TextModule
from hammerdraw.modules.warhammer_quest import HeroDiceSpaceModule, StatsModule, WeaponsModule, HeroRulesModule, BehaviourTableModule

class MinionCompiler(CompilerBase):
    modules = \
    [
        ImageModule,
        (TextModule, { 'name': "name", 'scale_field': "titleFontSizeScale" } ),
        StatsModule,
        WeaponsModule,
        
        HeroDiceSpaceModule,
        HeroRulesModule,
        BehaviourTableModule,
    ]
    module_name = 'warhammer_quest'
    compiler_type = "minion"
    
    def _get_base_filename(self):
        return super()._get_base_filename() \
            .replace('{weaponsCount}', str(len(self.raw['weapons'])))
