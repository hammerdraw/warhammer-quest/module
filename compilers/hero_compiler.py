from typing import Optional

from hammerdraw.compilers import CompilerBase
from hammerdraw.modules.core import ImageModule, TextModule
from hammerdraw.modules.warhammer_quest import StatsModule, WeaponsModule, HeroRulesModule, HeroDiceSpaceModule

class HeroCompiler(CompilerBase):
    modules = \
    [
        ImageModule,
        (TextModule, { 'name': "name_shade", 'field': 'name', 'scale_field': "titleFontSizeScale" } ),
        (TextModule, { 'name': "name", 'scale_field': "titleFontSizeScale" } ),
        (TextModule, { 'name': "subtitle_shade", 'field': 'subtitle', 'scale_field': "subtitleFontSizeScale" } ),
        (TextModule, { 'name': "subtitle", 'scale_field': "subtitleFontSizeScale" } ),
        StatsModule,
        WeaponsModule,
        
        HeroDiceSpaceModule,
        HeroRulesModule,
    ]
    module_name = 'warhammer_quest'
    compiler_type = "hero"
    
    def generate_filename(self):
        name: str = self.get_from_raw('name')
        subtitle: Optional[str] = self.get_from_raw('subtitle', default=None)
        if (subtitle is not None):
            name = f'{name}-{subtitle}'
        return self._generate_filename(name)
    
    def _get_output_name(self):
        subtitle: Optional[str] = self.get_from_raw('subtitle', default=None)
        name = super()._get_output_name()
        if (subtitle is not None):
            name = f'{name} {subtitle}'
        return name
    
    def _get_base_filename(self):
        return super()._get_base_filename() \
            .replace('{weaponsCount}', str(len(self.raw['weapons'])))
