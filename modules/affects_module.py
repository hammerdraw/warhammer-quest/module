from hammerdraw.modules.core import TextModule
from hammerdraw.text_drawer.text_funcs import capitalize_first
from hammerdraw.util import join_list

class AffectsModule(TextModule):
    module_name = 'affects'
    
    def initialize(self, **kwargs):
        super(AffectsModule, self).initialize(field='additionalEffect', multiline=True, **kwargs)
    
    def _compile(self, base):
        bonus_text = self.parent.get_from_raw(self.raw_field)
        if (not bonus_text):
            return 0
        
        traits_str = join_list(self.parent.get_from_raw('affects'), last_separator=' or ', formatter=lambda s: f"**{capitalize_first(s)}**")
        text = f"If your hero is {traits_str}, {bonus_text}"
        return self._print(base, text)

__all__ = \
[
    'AffectsModule',
]
