from .stats_module import StatsModule
from .weapons_module import WeaponsModule
from .behaviour_table_module import BehaviourTableModule
from .heroic_rules_module import HeroRulesModule
from .dice_space_module import HeroDiceSpaceModule
from .adversary_rules_module import AdversaryRulesModule
from .affects_module import AffectsModule
from .categories_module import CategoriesModule
