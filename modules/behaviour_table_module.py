from typing import List

from PIL import Image, ImageDraw, ImageColor

from hammerdraw.compiler_modules import ModuleBase, module_attribute
from hammerdraw.util import get_color, RawType

@module_attribute('behaviour_body_font_size_scale', 1.0)
@module_attribute('behaviour_header_font_size_scale', 1.0)
@module_attribute('behaviour_table/table')
@module_attribute('behaviour_table/title', "Behaviour Table")
@module_attribute('behaviour_table/dices', "D6")
class BehaviourTableModule(ModuleBase):
    module_name = "behaviour"
    behaviour_body_font_size_scale: float
    behaviour_header_font_size_scale: float
    behaviour_table_table: RawType
    behaviour_table_title: str
    behaviour_table_dices: str
    
    def get_size(self):
        _width = self.get_from_config('width')
        _height = self.get_from_config('heightMax')
        size = (_width, _height)
        return size
    
    def _compile(self, base):
        td = self.get_text_drawer(base, font_prefix='fonts/tableBody')
        _scale = self.behaviour_body_font_size_scale
        if (_scale != 1.0):
            td_font = td.get_font()
            td.set_font(font_size=td_font['font_size'] * _scale)
        
        th = self.get_text_drawer(base, font_prefix='fonts/tableHeader')
        _scale = self.behaviour_header_font_size_scale
        if (_scale != 1.0):
            th_font = th.get_font()
            th.set_font(font_size=th_font['font_size'] * _scale)
        
        behaviour_table_height = self.parent.insert_table \
        (
            vertical_columns = self.get_from_config("tableVerticalColumns"),
            top = -self.height - (self.get_from_config("tableBottomOffset") + self.get_from_config("borderBottom")),
            cell_height = 0,
            data = self.behaviour_table_table,
            
            body_row_template = [ "$$HA_C **{roll}**", "**{name}:** {description}" ],
            body_text_drawer = td,
            body_row_interval = self.get_from_config("tableBodyRowInterval"),
            
            header_row = [ "$$HA_C " + self.behaviour_table_dices, "Actions" ],
            header_text_drawer = th,
            header_bold = True,
        )
        
        x1 = self.get_from_config("borderLeft")
        x2 = self.get_from_config("borderRight")
        y2 = self.height + self.get_from_config("borderBottom")
        y1 = y2 + self.get_from_config("tableBottomOffset") - behaviour_table_height + self.get_from_config("borderTopOffset")
        rectangle_width = self.get_from_config("borderWidth")
        drawer = ImageDraw.ImageDraw(base)
        
        td = self.get_text_drawer(base, font_prefix='fonts/title')
        table_caption = self.behaviour_table_title
        td.print_in_region((x1, y1, x2, y1 + self.get_from_config("titleHeight")), text=table_caption, offset_borders=False)
        
        for i in range(rectangle_width):
            _i = i - rectangle_width // 2
            _x1 = x1 + _i
            _y1 = y1 + _i
            _x2 = x2 - _i
            _y2 = y2 - _i
            drawer.rectangle([(_x1, _y1), (_x2, _y2)], outline=get_color(self.get_from_config("borderColor")))
        behaviour_table_height -= self.get_from_config("borderTopOffset")
        
        self.logger.info("Behaviour table printed")
        if (hasattr(self.parent, 'behaviour_table_height')):
            self.parent.behaviour_table_height = behaviour_table_height
        
        return behaviour_table_height
