from hammerdraw.modules.core import TextModule
from hammerdraw.text_drawer.text_funcs import capitalize_first
from hammerdraw.util import join_list

class CategoriesModule(TextModule):
    module_name = 'categories'
    
    def initialize(self, **kwargs):
        super(CategoriesModule, self).initialize(field='categories', multiline=True, **kwargs)
    
    def _compile(self, base):
        categories = self.parent.get_from_raw(self.raw_field) or []
        item_type = self.parent.get_from_raw('itemType')
        if (item_type):
            categories = [ item_type ] + categories
        
        name = self.parent.get_from_raw('name')
        if (not categories):
            return 0
        
        # categories_str = join_list(categories, last_separator=' and ', formatter=lambda s: "**{0}**".format(s.capitalize()))
        # text = "{name} is {categories}.".format(name=name, categories=categories_str)
        text = join_list(categories, formatter=lambda s: "**{0}**".format(capitalize_first(s)))
        return self._print(base, text)

__all__ = \
[
    'CategoriesModule',
]
