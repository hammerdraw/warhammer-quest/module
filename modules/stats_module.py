from typing import Any, Dict

from hammerdraw.compiler_modules import ModuleBase

class StatsModule(ModuleBase):
    module_name = "stats"
    
    def _compile(self, base):
        td = self.get_text_drawer(base)
        
        _stats: Dict[str, Dict[str, Any]] = self.get_from_module_config('stats')
        for stat_name, stat_value in _stats.items():
            if (not (isinstance(stat_value, dict))):
                continue
            
            _x = stat_value['x']
            _y = stat_value['y']
            _text_template = "{{{statName}}}{plusSymbol}".format(statName=stat_name, plusSymbol='+' if stat_value['type'] == 'dice' else '')
            _text = _text_template.format_map(self.parent.raw['stats'])
            td.print_line((_x, _y), _text)
        
        self.logger.info("Stats printed")
        return 0

__all__ = \
[
    'StatsModule',
]
