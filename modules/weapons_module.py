from hammerdraw.compiler_modules import ModuleBase
from hammerdraw.text_drawer import TextDrawer, CapitalizationModes

class WeaponsModule(ModuleBase):
    module_name = "weapons"
    
    weapon_stats = None
    def initialize(self, **kwargs):
        self.weapon_stats = self.get_from_module_config("stats")
        self.__last_weapons_count = len(self.parent.get_from_raw('weapons', ''))
        super().initialize(**kwargs)
    
    def get_size(self):
        width = self.get_from_module_config("width")
        
        _cell_height = self.get_from_module_config("cellHeight")
        _weapons_count = len(self.parent.get_from_raw("weapons", ''))
        _body_row_interval = self.get_from_module_config("rowIntervalByCount")[_weapons_count]
        _header_row_interval = self.get_from_module_config("headerRowInterval")
        height = _cell_height * (_weapons_count + 1) + _header_row_interval + _body_row_interval * (_weapons_count - 1)
        
        return width, height
    
    def _compile(self, base):
        td = self.get_text_drawer(base)
        
        if (not self.parent.get_from_raw('weapons')):
            return 0
        
        total_height = self.parent.insert_table \
        (
            vertical_columns = self.get_from_module_config("verticalColumns"),
            top = 0,
            cell_height = self.get_from_module_config("cellHeight"),
            data = self.parent.get_from_raw('weapons'),
            
            body_row_template = self.get_from_module_config("bodyRowTemplate"),
            body_text_drawer = td,
            body_row_interval = self.get_from_module_config("rowIntervalByCount")[len(self.parent.raw['weapons'])],
            body_capitalization = CapitalizationModes.CapitalizeFirst,
            
            header_row = self.get_from_module_config("headerRow"),
            header_text_drawer = td,
            header_row_interval = self.get_from_module_config("headerRowInterval"),
            header_capitalization = CapitalizationModes.Normal,
        )
        self.logger.info("Weapon table printed")
        
        return total_height

__all__ = \
[
    'WeaponsModule',
]
